const express = require('express');
const { db } = require('./db/model')
const app = express();
const path = require('path')
const { addCat, allCats, findCatWithId, findAgedCats, deleteCat, updateCat } = require('./controller/cat')
const PORT = process.env.PORT || 8000;

app.set('view engine', 'hbs')
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.set('views', path.join(__dirname, '/views'))
app.use(express.static(path.join(__dirname, 'public')))

app.post('/addCat', async (req, res) => {
  const { cName, cAge, cBreed } = req.body
  try {
    const theCat = await addCat(cName, cAge, cBreed)
    res.status(201).send({ theCat })
  }
  catch (err) {
    res.status(400).send({ mess: "some error occured" })
  }
})
app.get('/allCats', async (req, res) => {
  const theCats = await allCats()
  res.status(201).send(theCats)
})

app.get('/findOne/:id', async (req, res) => {
  const { id } = req.params
  const Cat = await findCatWithId(id)
  if (Cat) {
    res.status(201).send(Cat)
  }
  else {
    res.status(201).send('Not found')
  }
})

app.get('/search', async (req, res) => {
  const start = req.query.age_gte
  const end = req.query.age_lte
  const Cats = await findAgedCats(start, end)
  if (Cats) {
    res.status(201).send(Cats)
  }
  else {
    res.status(201).send("Not found")
  }

})

app.delete('/deleteCat/:id', async (req, res) => {
  const { id } = req.params
  const result = await deleteCat(id)
  if (result) {
    res.send(true)
  }
  else {
    res.send(false)
  }

})

app.put('/updateCat/:id', async (req, res) => {
  console.log('req.body',req.body,'id',req.params.id)
  const { id } = req.params
  
  
  const g=parseInt(id[1])
  console.log('id',g)
  /* const Cat = await findCatWithId(g) */
  const { cName, cAge, cBreed } = req.body
  /* if(Cat){
    Cat.set({
      catName: "cName",
      catAge: "cAge",
      catBreed: "cBreed"
    });
    await Cat.save();
    res.send(true)
  }
  else{
    res.send(false)
  } */
  

 /*  res.send(true) */

  const theCat = await updateCat(g, cName, cAge, cBreed)
  
  if (theCat) {
    res.send(true)
  }
  else {
    res.send(false)
  }
})


db.sync()
  .then(() => {
    app.listen(PORT, () => {
      console.log(`listening on port http://localhost:${PORT}`)
    })
  })
  .catch((err) => {
    console.error(err)
  })
