const Sequelize = require('sequelize')
const Op = Sequelize.Op;
const db = new Sequelize({
    
    storage: __dirname +'/catsdb.db',
    dialect:'sqlite'
   
})


const Model = Sequelize.Model

db.authenticate()
.then(()=>{
    console.log('authenticated')
})
.catch((err)=>{
    console.log(err)
})

module.exports ={db,Model,Op}