const { db, Model } = require('./connection')
const { DataTypes } = require('sequelize')

const COL_ID_DEF = {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
}
class cats extends Model{}
cats.init({
    id:COL_ID_DEF,
    catName:{
        type: DataTypes.STRING,
        allowNull:false
    },
    catAge:{
        type: DataTypes.INTEGER,
        allowNull:false
    },
    catBreed:{
        type:DataTypes.STRING,
        allowNull:false
    }
},{sequelize:db,modelName:'cats'})

module.exports = {db,cats}