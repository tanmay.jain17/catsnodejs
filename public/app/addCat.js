let catName = $('#cName')
let catAge = $('#cAge')
let catBreed = $('#cBreed')
let btnAdd = $('#cbtnAdd')

btnAdd.click(function () {
    if (!(catName.val()) || !(catAge.val()) || !(catBreed.val())) {
        window.alert('Please fill all the details')
    }
    else {

        let cName = catName.val()
        let cAge = catAge.val()
        let cBreed = catBreed.val()
        console.log(cName, cAge, cBreed)
        $.post('/addCat',{cName:cName,cAge:cAge,cBreed:cBreed},(post)=>{
            
            window.alert('Your cat has been added')
        })
    }
})
