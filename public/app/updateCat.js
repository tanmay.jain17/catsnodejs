$.put = function(url, data, callback, type){
 
    if ( $.isFunction(data) ){
      type = type || callback,
      callback = data,
      data = {}
    }
   
    return $.ajax({
      url: url,
      type: 'PUT',
      success: callback,
      data: data,
      contentType: type
    });
  }
let catsName = $('#cName')
let catAge = $('#cAge')
let catId = $('#cId')
let catBreed = $('#cBreed')
let btnAdd = $('#cbtnAdd')

btnAdd.click(function () {
    if (!(catsName.val()) || !(catAge.val()) || !(catBreed.val())||!(catId.val())) {
        window.alert('Please fill all the details')
    }
    else {

        let cName = catsName.val()
        let cAge = catAge.val()
        let cBreed = catBreed.val()
        /* console.log(cName, cAge, cBreed,catId.val()) */
        $.put(`/updateCat/:${catId.val()}`,{cName:cName,cAge:cAge,cBreed:cBreed},(post)=>{
            if(post){
                window.alert('Your cat has been updated')
            }
            else{
                alert('Cat does not exist')
            }
            
        })
    }
})
