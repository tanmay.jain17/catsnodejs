function navBarFunc(){
    let links = $('.navbar-item')
    links.click(function(event){
        let tab = $(event.target)
        let urlComponent = tab.attr('data-component')
        
        $('#contents').load(`../components/${urlComponent}.html`)
    })
}
navBarFunc()