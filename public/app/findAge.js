let start= $('#startAge')
let end = $('#endAge')

let btnAdd = $('#btnFindAge')

btnAdd.click(function () {
    if (!(start.val()) ||!(end.val()) || start.val() > end.val())  {
        window.alert('Please enter correct age range')
    }
    else {

        
        const url = "/search"+`?age_gte=${start.val()}&age_lte=${end.val()}`
        $.get(url,(Cats)=>{
            if(Cats.length>0){
                $('#ageRangeCats').empty()
                console.log(Cats)
                for (let p of Cats) {
                    /* let body = p.body
                    const {theCats} = p.body
                    console.log(typeof(body)) */
                    /* let newBody = body.slice(0,200) */
        
        
                    $('#ageRangeCats').append(`
                    <tr>
                        <th>${p.id}</th>
                        <td>${p.catName}</td>
                        <td>${p.catAge}</td>
                        <td>${p.catBreed}</td>
                    </tr>
                `)
        
                }
            }
            else{
                alert(`Cat within age range ${start.val()} to ${end.val()} does not exist`)
            }
           

        })
        
        
        
    }
})