$.delete = function(url, data, callback, type){
 
    if ( $.isFunction(data) ){
      type = type || callback,
          callback = data,
          data = {}
    }
   
    return $.ajax({
      url: url,
      type: 'DELETE',
      success: callback,
      data: data,
      contentType: type
    });
  }
let catssId= $('#catsId')

let btnDelete = $('#btnDelete')

btnDelete.click(function () {
    if (!(catssId.val()) ) {
        window.alert('Please give the Cat Id')
    }
    else {

        let catsId= catssId.val()
        const url = "/deleteCat"+`/${catsId}`
        $.delete(url,(Cat)=>{
            if(Cat){
                alert(`Cat with id ${catsId} is deleted`)
            }
            else{
                alert(`Cat with id ${catsId} does not exist`)
            }
           

        })
        
        
        
    }
})
