const { cats } = require('../db/model')
const { Op } = require('../db/connection')

async function addCat(name, age, breed) {
    try {
        const theCat = await cats.create({
            catName: name,
            catAge: age,
            catBreed: breed
        })
        return theCat
    }
    catch (err) {
        console.error(err)
        return err
    }
}

async function allCats() {
    try {
        const theCats = await cats.findAll()
        return theCats
    }
    catch (err) {
        console.error(err)
        console.log(err)
        return err
    }
}

async function findCatWithId(id) {
    const theCat = await cats.findOne({
        where: {
            id: id
        }
    })
    if (theCat) {
        return theCat
    }
    else {
        return false
    }
}

async function findAgedCats(start, end) {
    const allTheCats = await cats.findAll({
        where: {
            catAge: {
                [Op.gte]: start,
                [Op.lte]: end
            }
        }
    })
    if (allTheCats.length > 0) {
        return allTheCats
    }
    else {
        return false
    }
}

async function deleteCat(id) {
    try {
        const result = await cats.destroy({
            where: {
                id: id
            }
        })
        if (result) {
            console.log(result)
            return result
        }
        else {
            return false
        }
    }
    catch (err) {
        console.err(err)
        return err
    }

}
async function updateCat(id, name, age, breed) {
    try {
        const theCat = await cats.update({
            catName: name,
            catAge: age,
            catBreed: breed,
        }, {
            where: {
                id: id
            }
        })
        if (theCat[0]) {
            console.log(theCat)
            return theCat
        }
        else {
            console.log(theCat)
            return false
        }
    }
    catch (err) {

        return err
    }
}

module.exports = { addCat, allCats, findCatWithId, findAgedCats, deleteCat, updateCat }